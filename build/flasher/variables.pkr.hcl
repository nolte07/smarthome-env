
variable "image_home_dir" {
  type = string
  default = "/home/pi"
}

variable "image_base" {
  type = string
  default = "/build/output-arm-image/rpibase.img"
}

variable "iso_md5sum" {
  type = string
  default = "e5ca21f6826f6d7dc51a480643c2f561"
}
