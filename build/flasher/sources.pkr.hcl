source "arm-image" "basement" {
  iso_url = "file://${var.image_base}"
  output_filename = "../output-arm-image/hostapd.img"
  target_image_size = 3*1024*1024*1024
  image_type = "raspberrypi"
  iso_checksum_type = "md5"
  iso_checksum = "${var.iso_md5sum}"
}
