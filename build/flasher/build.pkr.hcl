
build {
  sources = [
    "source.arm-image.basement"
  ]
  # install hostapd, bridge utils, and other utilities.
  provisioner "shell" {
    inline = [
      "apt-get update",
    ]
  }
  # upload all our configuration files.
  provisioner "shell" {
    inline = [
      "cd /home/pi/",
      "git clone https://github.com/ct-Open-Source/tuya-convert",
      "cd tuya-convert",
      "./install_prereq.sh",
      "chown -R pi:pi /home/pi/tuya-convert",
    ]
  }
}
