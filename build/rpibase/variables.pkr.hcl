
variable "image_home_dir" {
  type = string
  default = "/home/pi"
}

variable "wifi_ssid" {
  type = string
}
variable "wifi_psk" {
  type = string
  sensitive = true
}

variable "wpa_supplicant_country" {
  type = string
  default = "DE"
}

variable "local_ssh_public_key" {
  type = string
  default = "~/.ssh/id_ed25519.pub"
}
  
locals {
  ssh_key = "${pathexpand(var.local_ssh_public_key)}"
}