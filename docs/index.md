---
layout: "smarthome"
page_title: "Home Automation: k8s"
sidebar_current: "docs-smarthome-index"
description: |-
  Smart Home Env
---

# Home Automation: k8s

Die Hausautomatisierung basiert auf einer kombination von [home-assistant.io](https://home-assistant.io) für die grundlegende Steuerung, sowie unterschiedlichste Integrationen, z.B. [esphome.io](https://esphome.io) und/oder [tasmota.github.io](https://tasmota.github.io/docs/).  
Dabei sollte das Ziel immer sein das ein Großteil des SmartHome Systems auch im falle eines Internet Ausfalls verfügbar ist.

Der Betrieb des SmartHome Systems findet auf einem "lokalen" [rancher/k3s](https://github.com/rancher/k3s) Hybrid Cluster statt. 
Für die Konfiguration von home-assistant (hass) wird ein privates Repo genutzt, [nolte/home-assistant-config](https://github.com/nolte/home-assistant-config).

## Links

* [homeassistant.jan-kuepper.de](http://homeassistant.jan-kuepper.de/)