---
layout: "smarthome"
page_title: "Base Image"
sidebar_current: "docs-image-index"
description: |-
  Smart Home Env
---


# ArmImages on X64

[solo-io/packer-builder-arm-image](https://github.com/solo-io/packer-builder-arm-image)


## Privates Image

Das gebaute image wird um Wlan und grundlegende Authentication Configurationen erweitert. Ziel ist es das der RPI direkt im privaten Netzwerk verfügbar ist. 
Als beispiel nutzen wir die Implementierung von [solo-io/packer-builder-arm-image/samples](https://github.com/solo-io/packer-builder-arm-image/tree/master/samples)

**Vorbedingung**

* Existierender SSH Key
* Eine Funktionsfähige Docker Umgebung (Tested on Ubuntu 18.04)

### Build the Iso

Das Original Image wird um einiege Einstellungen erweitert:

* Anmeldung über SSH
* Deaktivierung der Passwort Anmeldung
* Hinterlegen eines Pulic Keys für den `pi` user.

```bash
docker run \
  --rm \
  --privileged \
  -v ${PWD}:/build:ro \
  -v ${PWD}/packer_cache:/build/packer_cache \
  -v ${PWD}/output-arm-image:/build/output-arm-image \
  -v ${HOME}/.ssh/id_ed25519.pub:/config/id_ed25519.pub:ro \
  -e PACKER_CACHE_DIR=/build/packer_cache \
  -w /build/rpibase \
  quay.io/solo-io/packer-builder-arm-image:v0.1.5 build \
      -var local_ssh_public_key=/config/id_ed25519.pub \
      -var wifi_ssid=$(pass private/wlan/home/ssid) -var wifi_psk=$(pass private/wlan/home/psk) .
```

Das erzeugte Image wird für alle Installierten RPI Devices verwendet. Die nachgelagerte _fein_ Konfiguration findet nachgelagert über [Ansible](https://www.ansible.com/) und [Terraform](https://www.terraform.io) statt.

### Flash the SDCard


## Links 

* https://itnext.io/deploying-a-microservice-oriented-application-to-kubernetes-from-zero-to-production-416a173a8505