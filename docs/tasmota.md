---
layout: "smarthome"
page_title: "tasmota"
sidebar_current: "docs-tasmota-index"
description: |-
  Smart Home Env
---

Eine alternative den oft sehr fragwürdiegen Firmewares ist [tasmota](https://tasmota.github.io/docs/).  
Für das Flasher der Devices nutzen wir [ct-Open-Source/tuya-convert](https://github.com/ct-Open-Source/tuya-convert).



## Prepare SD Card

```bash
docker run \
  --rm \
  --privileged \
  -v ${PWD}:/build:ro \
  -v ${PWD}/packer_cache:/build/packer_cache \
  -v ${PWD}/output-arm-image:/build/output-arm-image \
  -e PACKER_CACHE_DIR=/build/packer_cache \
  -w /build/flasher \
  quay.io/solo-io/packer-builder-arm-image:v0.1.5 build -var iso_md5sum=$(md5sum output-arm-image/rpibase.img | awk '{ print $1 }') .
```

Das erzeuge image unter `/build/output-arm-image/hostapd.img` kann direkt für das Flasher der Devices Genutzt werden. 
Der Geflashte RPI Sollte per Kabel mit dem Heimnetz verbunden sein. Da ihr zum Flasher der einzelnen [Produkte](https://templates.blakadder.com/all.html) einen Wlan Acces Point braucht.

## Used for

* [Gosund P1 Power Monitoring Power Strip](https://templates.blakadder.com/gosund_P1.html)
* [Gosund SP111 Power Monitoring Plug](https://templates.blakadder.com/gosund_SP111.html)
* [Xiaomi Philips MUE4088RT Dimmable Bulb](https://templates.blakadder.com/xiaomi_philips_MUE4088RT.html) _(planed)_

## Checklist New Devices

* Die [Haas](https://tasmota.github.io/docs/Home-Assistant/#enabling) Integration findet über einen MQTT Broker statt. (`SetOption19 1`)
* Anpassen der MQTT Config (Hinterlegen Host,anpassen des ports auf den NodePort)
* Festlegen eines Device Namens unter `configure -> other`
