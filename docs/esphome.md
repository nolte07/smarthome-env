---
layout: "esphome"
page_title: "esphome Image"
sidebar_current: "docs-esphome-index"
description: |-
  Smart Home Env
---

Die Firmware [esphome](https://esphome.io/) eignet sich wunderbar für eigene Projekte, und lässt sich deutlich besser individualisieren als [tasmota](./tasmota.md).  
Das Flashen von ESPs kann durch einen [Container](https://esphome.io/guides/faq.html#docker-reference) geschehen siehe [Getting Started with ESPHome](https://esphome.io/guides/getting_started_command_line.html)  

Beisielhafte Usecases:

* [Sensor Station](./integrations/diy/sensor-station.md), Tracken von BT geräten und erfassen von grundlegenden Rauminformationen (Temperatur, luftfeuchtigkeit etc...)