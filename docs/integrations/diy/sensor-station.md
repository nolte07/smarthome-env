---
layout: "esphome"
page_title: "Sensor Station"
sidebar_current: "docs-diy-sensor-station-index"
description: |-
  ESH Home Sensore Station
---

# Living Room Station

Eine Einfache ESP Einheit, 
```bash
docker run --rm \
    --device=/dev/ttyUSB0  \
    -e WIFI_SSID=$(pass private/wlan/home/ssid) \
    -e WIFI_PASSWORD=$(pass private/wlan/home/psk) \
    -e WIFI_DOMAIN=".fritz.box" \
    -v "${PWD}":/config \
    -it esphome/esphome floortrack.yaml run
```


```bash
docker run --rm \
    -e WIFI_SSID=$(pass private/wlan/home/ssid) \
    -e WIFI_PASSWORD=$(pass private/wlan/home/psk) \
    -e WIFI_DOMAIN=".fritz.box" \
    -v "${PWD}":/config \
    -it esphome/esphome livingroom.yaml logs
```

## Sensor

```bash
hcitool scan
```

## Links

* [esphome_dht](https://esphome.io/components/sensor/dht.html)
* [esphome_xiaomi_hhccjcy01](https://esphome.io/components/sensor/xiaomi_hhccjcy01.html)
* [esphome_deep_sleep](https://esphome.io/components/deep_sleep.html)
* [esp32-bluetooth-low-energy](https://randomnerdtutorials.com/esp32-bluetooth-low-energy-ble-arduino-ide/)