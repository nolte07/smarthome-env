
variable "namespace" {
  
}

resource "helm_release" "helm" {
  name       = "mosquitto"
  repository = "https://storage.googleapis.com/t3n-helm-charts"
  chart      = "mosquitto"
  version    = "0.1.1"
  namespace  = var.namespace
  values = [
    "${templatefile("${path.module}/files/values.yml", {
    })}"
  ]

  timeout = 900
}


variable "depends_list" {
  default = []
}

output "depend_on" {
  # list all resources in this module here so that other modules are able to depend on this
  value = [helm_release.helm.id]
}
