resource "kubernetes_namespace" "homeassistant" {
  metadata {
    name = "homeassistant"
  }
}


module "mqtt" {
  source = "./mqtt"
  namespace = kubernetes_namespace.homeassistant.metadata[0].name
}

module "homeassistant" {
  depends_list = [module.mqtt.depend_on]
  source = "./homeassistant"
  namespace = kubernetes_namespace.homeassistant.metadata[0].name
}
