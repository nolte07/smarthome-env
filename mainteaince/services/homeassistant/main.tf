
variable "namespace" {
  
}
variable "domain_name" {
  default = "192-168-178-88.sslip.io"
}




data "pass_password" "google_client_id" {
  path = "network/homeassistant/google/clientid"
}
data "pass_password" "google_client_secret" {
  path = "network/homeassistant/google/clientsecret"
}
data "pass_password" "fritz_box_user" {
  path = "network/homeassistant/fritzbox/user"
}
data "pass_password" "fritz_box_password" {
  path = "network/homeassistant/fritzbox/password"
}
data "pass_password" "openweathermap_api_key" {
  path = "network/homeassistant/openweather/apikey"
}
resource "kubernetes_secret" "home_assistant_secrets" {
  depends_on = [var.depends_list]
  metadata {
    name      = "home-assistant-secrets"
    namespace = var.namespace
  }

  data = {
    GOOGLE_CLIENT_ID       = data.pass_password.google_client_id.full
    GOOGLE_CLIENT_SECRET   = data.pass_password.google_client_secret.full
    FRITZBOX_ENDPOINT      = "192.168.178.1"
    FRITZBOX_USERNAME      = data.pass_password.fritz_box_user.full
    FRITZBOX_PASSWORD      = data.pass_password.fritz_box_password.full
    DENON_ENDPOINT         = "192.168.178.24"
    FIRETV_ENDPOINT        = "192.168.178.27"
    LANNOUNCER_ENDPOINT    = "192.168.178.71"
    OPENWEATHERMAP_API_KEY = data.pass_password.openweathermap_api_key.full
    MQTT_ENDPOINT          = "mosquitto"
    HUE_ENDPOINT           = "192.168.178.41"
  }
}



data "pass_password" "ssh_id_rsa" {
  path = "internet/project/homeassistant/deploymentkey/id_rsa"
}

data "pass_password" "ssh_id_rsa_pub" {
  path = "internet/project/homeassistant/deploymentkey/id_rsa.pub"
}

resource "kubernetes_secret" "git_creds" {
  depends_on = [var.depends_list]
  metadata {
    name      = "git-creds"
    namespace = var.namespace
  }

  data = {
    id_rsa       = data.pass_password.ssh_id_rsa.full
    known_hosts  = file("${path.module}/files/known_hosts")
    "id_rsa.pub" = data.pass_password.ssh_id_rsa_pub.full
  }

}

resource "helm_release" "homeassistant" {
  depends_on = [kubernetes_secret.git_creds,kubernetes_secret.home_assistant_secrets]
  name       = "homeassistant"
  repository = "https://billimek.com/billimek-charts/"
  chart      = "home-assistant"
  version    = "1.0.0"
  namespace  = var.namespace
  values = [
    "${templatefile("${path.module}/files/values.yml", {
      DOMAIN = var.domain_name
    })}"
  ]

  timeout = 900
}



variable "depends_list" {
  default = []
}

output "depend_on" {
  # list all resources in this module here so that other modules are able to depend on this
  value = [
    kubernetes_secret.git_creds,
    helm_release.homeassistant.id,
    kubernetes_secret.home_assistant_secrets,
    ]
}
